package com.Base;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Baseclass {
	

	public static WebDriver driver;
	public static Properties prop = new Properties();
	public static FileInputStream File;
	
	@BeforeTest
	public void setup() throws Throwable {
		
		FileInputStream fis = new FileInputStream("C:\\Users\\SSC\\eclipse-workspace\\HIRING\\src\\test\\resources\\Data.properties");
		
		prop.load(fis);
		
		/*String URL = prop.getProperty("url");
		String Username = prop.getProperty("Username");
		String Password = prop.getProperty("password");*/
		
		if(prop.getProperty("Browser").equalsIgnoreCase("chrome"))
		{
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
			driver.get(prop.getProperty("url1"));
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			
		} 
		else if(prop.getProperty("Browser").equalsIgnoreCase("firefox"))
		{
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
			driver.get(prop.getProperty("url"));
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			
		}
	}
	@AfterTest
	public void tearDown()
	{
		driver.close();
	}
	
	}

	
		
	


