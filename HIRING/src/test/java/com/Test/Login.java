package com.Test;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.Base.Baseclass;

public class Login extends Baseclass {
	
	@Test
	public static void loginFunction() throws InterruptedException
	{
		WebDriverWait wait = new  WebDriverWait(driver, Duration.ofSeconds(10));
		
	   driver.manage().timeouts().implicitlyWait(Duration.ofMillis(10));
		
        driver.findElement(By.xpath("//*[@fill='#2f2f2f']")).click();//for microsoft 
		Thread.sleep(5000);
		
	    driver.findElement(By.xpath("//input[@id='i0116']")).sendKeys(prop.getProperty("Username"));//for mail id
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='idSIButton9']")));
	    
		
		driver.findElement(By.xpath("//input[@id='idSIButton9']")).click();//next button
		
		driver.findElement(By.xpath("//input[@name='passwd']")).sendKeys(prop.getProperty("password"));//password field
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='idSIButton9']")));
		
		try
		{
		driver.findElement(By.xpath("//input[@id='idSIButton9']")).click();//next button on password field
		}
		catch(StaleElementReferenceException e)
		{
			driver.findElement(By.xpath("//input[@id='idSIButton9']")).click();
		}
		Thread.sleep(5000);
		driver.findElement(By.xpath("//input[@id='KmsiCheckboxField']")).click();//check box
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@type='submit']")));
		
		
		driver.findElement(By.xpath("//input[@type='submit']")).click();//yes button
	
	}
}



